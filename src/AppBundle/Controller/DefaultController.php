<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Document\Movie;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\ODM\MongoDB\Cursor;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET|POST")
     */
    public function indexAction(Request $request)
    {
      $repository = $this->get('doctrine_mongodb')->getManager()->getRepository('AppBundle:Movie');

      if ($request->request->get('search') != '' || $request->request->get('search') != null) {
        $search = $request->request->get('search');

        //pagination
        $query = $this->get('doctrine_mongodb')->getManager()->getRepository('AppBundle:Movie')->findBy(
          [
            'title' => new \MongoRegex("/^$search/i")
          ]
        );

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            12/*limit per page*/
        );
      }else{
        $search = "";
        //pagination
        // $query = $this->get('doctrine_mongodb')->getManager()->getRepository('AppBundle:Movie')->findAll();
        $query = $this->get('doctrine_mongodb')->getManager()->getRepository('AppBundle:Movie')->findBy(
          []
        );

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            12/*limit per page*/
        );
      }

      return $this->render('Accueil/index.html.twig', [
          'search' => $search,
          'pagination' => $pagination,
      ]);
    }

    /**
     * @Route("/ajouter", name="ajouter")
     * @Method("GET|POST")
     */
    public function addAction(Request $request)
    {
      if ($request->request->get('submit') == 'submit') {
        $movie = new Movie();

        $movie->setTitle($request->request->get('title').' ('.$request->request->get('annee').')');
        $movie->setImdbId($request->request->get('idImdb'));
        $movie->setLink($request->request->get('link'));
        $movie->setScore($request->request->get('score'));
        $movie->setGenre($request->request->get('genre'));
        $movie->setImg($request->request->get('img'));

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($movie);
        $dm->flush();

        return $this->redirectToRoute('homepage');
      }
      return $this->render('Accueil/add.html.twig', [

      ]);
    }

    /**
    * @Route("/administration", name="administration")
    */
    public function administrationAction(Request $request)
    {
      $movies = $this->get('doctrine_mongodb')->getManager()->getRepository('AppBundle:Movie')->findAll();

      return $this->render('Accueil/admin.html.twig', [
        'movies' => $movies
      ]);
    }

    /**
    * @Route("/modifier/{id}", name="update", requirements={"id"="[a-zA-Z0-9_]+"})
    * @Method("GET|POST")
    */
    public function updateAction(Request $request, $id)
    {
      $movie = $this->get('doctrine_mongodb')->getManager()->getRepository('AppBundle:Movie')->find($id);

      if ($request->request->get('submit') == 'submit') {
        $movie->setTitle($request->request->get('title'));
        $movie->setImdbId($request->request->get('idImdb'));
        $movie->setLink($request->request->get('link'));
        $movie->setScore($request->request->get('score'));
        $movie->setGenre($request->request->get('genre'));
        $movie->setImg($request->request->get('img'));

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($movie);
        $dm->flush();

        return $this->redirectToRoute('homepage');
      }

      return $this->render('Accueil/update.html.twig', [
        'movie' => $movie,
      ]);
    }

    /**
    * @Route("/delete/{id}", name="delete", requirements={"id"="[a-zA-Z0-9_]+"})
    * @Method("GET|POST")
    */
    public function deleteAction(Request $request, $id)
    {
      $movie = $this->get('doctrine_mongodb')->getManager()->getRepository('AppBundle:Movie')->find($id);

      if ($request->request->get('submitdelete') == 'submitdelete') {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->remove($movie);
        $dm->flush();

        return $this->redirectToRoute('homepage');
      }
    }

    /**
    * @Route("/sort/{mess}", name="sort", requirements={"mess"="[a-zA-Z0-9_]+"})
    */
    public function sortAction(Request $request, $mess)
    {
      var_dump($mess);
      exit;
    }
}
