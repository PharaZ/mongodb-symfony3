<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Film
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="integer")
     */
    protected $year;

    /**
     * @MongoDB\Field(type="integer")
     */
    protected $length;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $title;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $subject;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $actor;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $actress;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $director;

    /**
     * @MongoDB\Field(type="integer")
     */
    protected $popularity;

    /**
     * @MongoDB\Field(type="boolean")
     */
    protected $awards;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $images;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return $this
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * Get year
     *
     * @return integer $year
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set length
     *
     * @param integer $length
     * @return $this
     */
    public function setLength($length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * Get length
     *
     * @return integer $length
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Get subject
     *
     * @return string $subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set actor
     *
     * @param string $actor
     * @return $this
     */
    public function setActor($actor)
    {
        $this->actor = $actor;
        return $this;
    }

    /**
     * Get actor
     *
     * @return string $actor
     */
    public function getActor()
    {
        return $this->actor;
    }

    /**
     * Set actress
     *
     * @param string $actress
     * @return $this
     */
    public function setActress($actress)
    {
        $this->actress = $actress;
        return $this;
    }

    /**
     * Get actress
     *
     * @return string $actress
     */
    public function getActress()
    {
        return $this->actress;
    }

    /**
     * Set director
     *
     * @param string $director
     * @return $this
     */
    public function setDirector($director)
    {
        $this->director = $director;
        return $this;
    }

    /**
     * Get director
     *
     * @return string $director
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * Set popularity
     *
     * @param integer $popularity
     * @return $this
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;
        return $this;
    }

    /**
     * Get popularity
     *
     * @return integer $popularity
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * Set awards
     *
     * @param boolean $awards
     * @return $this
     */
    public function setAwards($awards)
    {
        $this->awards = $awards;
        return $this;
    }

    /**
     * Get awards
     *
     * @return boolean $awards
     */
    public function getAwards()
    {
        return $this->awards;
    }

    /**
     * Set images
     *
     * @param string $images
     * @return $this
     */
    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * Get images
     *
     * @return string $images
     */
    public function getImages()
    {
        return $this->images;
    }
}
