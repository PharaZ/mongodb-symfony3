Mongodb + Symfony 3
========================

Ce projet est un listage d'une base de données de films (40000).
Il a été réalisé avec Symfony 3 et mongodb.

Prérequis avant l'installation
--------------

- Installer mongodb
- installer php 7.1
- installer composer


Installer le projet et le lancer
--------------

- Récupérer le projet avec la commande : git clone https://gitlab.com/PharaZ/mongodb-symfony3.git
- Une fois téléchargé lancer la commande : composer install
- Ensuite lancer mongodb avec la commande : mongod
- Ensuite importer le fichier json (/web/movie.json) avec la commande : mongoimport --db film_database --collection Movie --file movie.json --jsonArray
- Une fois importer vous pouvez lancer la commande : php bin/console server:run